package com.prueba.liverpool.interfaces

import com.prueba.liverpool.models.MProductMain

interface IProduct {
    interface View {
        fun successProduct(model: MProductMain)
        fun errorProduct(error: String)
    }

    interface Presenter {
        fun searchProduct(query: String, startPage: Int, endPage: Int)
    }

}
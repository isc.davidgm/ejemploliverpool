package com.prueba.liverpool.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.prueba.liverpool.R
import com.prueba.liverpool.models.MProduct
import com.prueba.liverpool.util.Tools
import kotlinx.android.synthetic.main.card_product.view.*

class ProductAdapter(val context: Context, var arrayList: Array<MProduct>) :
    RecyclerView.Adapter<ProductAdapter.Holder>() {

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindView(arrayList[position])
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.card_product, parent, false))
    }

    class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        fun bindView(m: MProduct) {
            itemView.cpTvTitle.text = m.productDisplayName
            itemView.cpTvDescription.text = m.seller
            itemView.cpTvPrice.text = Tools.getMoney(m.listPrice)

            if (m.smImage.isNullOrEmpty()) {
                itemView.cpIvImage.visibility = View.GONE
            } else {
                itemView.cpIvImage.visibility = View.VISIBLE
                var requestOptions = RequestOptions()
                requestOptions = requestOptions.transforms(CenterInside(), RoundedCorners(16))
                Glide
                    .with(itemView.context)
                    .load(m.smImage)
                    .apply(requestOptions)
                    .into(itemView.cpIvImage)
            }
        }
    }
}
package com.prueba.liverpool.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.prueba.liverpool.R
import com.prueba.liverpool.models.MCacheHistory
import com.prueba.liverpool.views.VHome
import kotlinx.android.synthetic.main.card_history_search.view.*

class HistorySearchAdapter(val context: Context, var arrayList: ArrayList<MCacheHistory>) :
    RecyclerView.Adapter<HistorySearchAdapter.Holder>() {

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindView(arrayList[position])
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.card_history_search, parent, false))
    }

    class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        fun bindView(m: MCacheHistory) {
            itemView.chsTvQuery.text = m.query

            itemView.setOnClickListener { (itemView.context as VHome).setQueryAdapter(m.query) }
        }
    }
}
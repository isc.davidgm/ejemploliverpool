package com.prueba.liverpool.network

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.gson.GsonBuilder
import com.prueba.liverpool.models.MResponse
import com.prueba.liverpool.util.DefaultExclusionStrategy
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList
import java.util.concurrent.TimeUnit

class Petition {
    companion object {
        const val TAG_IS_OK = "OK"

        inline fun <reified T> responseModel(response: String): T {
            val builder = GsonBuilder()
            builder.setExclusionStrategies(DefaultExclusionStrategy())
            val gSon = builder.create()
            return gSon.fromJson(response, T::class.java)
        }

        inline fun <reified T> responseListJson(response: JSONArray): ArrayList<T> {
            val builder = GsonBuilder()
            builder.setExclusionStrategies(DefaultExclusionStrategy())
            return try {
                val arrayList = ArrayList<T>()
                for (i in 0 until response.length()){
                    val m = response.get(i)
                    val newModel = responseModel<T>(m.toString())
                    arrayList.add(newModel)
                }
                arrayList
            } catch (e: Exception) {
                ArrayList()
            }
        }

        inline fun petitionGet(
            endPoint: String,
            crossinline onRes: (r: JSONObject) -> Unit,
            crossinline onErr: (e: String) -> Unit
        ) {
            AndroidNetworking.get(endPoint)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        val res = MResponse().response(response.toString())
                        return try {
                            when {
                                res!!.isOk -> {
                                    onRes(res!!.dsResponse!!)
                                }
                                else -> {
                                    onErr(
                                        "Aquí se valida un error"
                                    )
                                }
                            }
                        } catch (e: Exception) {
                            onErr("Aquí se valida un error"
                            )
                        }
                        return onErr("Aquí se valida un error")
                    }

                    override fun onError(anError: ANError) {
                        var error = anError.errorBody
                        if (error.isNullOrEmpty())
                            error = "Aquí se valida un error"
                        return onErr(error)
                    }
                })
        }
    }
}
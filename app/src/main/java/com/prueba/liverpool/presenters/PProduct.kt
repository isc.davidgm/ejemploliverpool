package com.prueba.liverpool.presenters

import android.util.Log
import com.prueba.liverpool.interfaces.IProduct
import com.prueba.liverpool.models.MProduct
import com.prueba.liverpool.models.MProductMain
import com.prueba.liverpool.network.Petition
import com.prueba.liverpool.network.WebServices

class PProduct(view: IProduct.View) : IProduct.Presenter {

    private var view: IProduct.View? = view

    override fun searchProduct(query: String, startPage: Int, endPage: Int) {
        if (view != null) {

            Petition.petitionGet(
                WebServices.SEARCH_URL+"force-plp=true&search-string=$query&page-number=$startPage&number-of-items-per-page=$endPage".replace(" ", "%20"),
                onRes = { r ->
                    var m = Petition.responseModel<MProductMain>(r.toString())
                    view?.successProduct(m)
                },
                onErr = { e -> view?.errorProduct(e) }
            )
        }
    }
}
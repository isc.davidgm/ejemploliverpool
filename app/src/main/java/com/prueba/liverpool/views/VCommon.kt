package com.prueba.liverpool.views

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.prueba.liverpool.models.MCacheHistory
import org.json.JSONArray


open class VCommon: AppCompatActivity() {

    private val PREFERENCES_FILE = "Liverpool.HistoryCache"

    val TAG_SEARCH_CACHE = "TAG_SEARCH_CACHE"

    fun setSearchCache(query: String) {
        var arr = getSearchCache()
        var isAdd = true
        for (i in arr){
            if (query.toLowerCase() == i.query.toLowerCase()){
                isAdd = false
                break
            }
        }
        if (isAdd && !query.isNullOrEmpty()){
            var m = MCacheHistory()
            m.query = query
            arr.add(m)
        }

        val preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
        val edit = preferences.edit()
        val gson = Gson()

        val jsonString = gson.toJson(arr)
        edit.putString(TAG_SEARCH_CACHE, jsonString)
        edit.commit()
    }

    fun getSearchCache(): ArrayList<MCacheHistory> {
        val preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
        val gsonString = preferences.getString(TAG_SEARCH_CACHE, "")
        if (gsonString.isNullOrEmpty()){
            return ArrayList()
        } else {
            var res = ArrayList<MCacheHistory>()
            val builder = GsonBuilder()
            val gson = builder.create()

            try {
                val arrJson = JSONArray(gsonString)
                for (i in 0 until arrJson.length()) {
                    var m= gson.fromJson(arrJson[i].toString(),MCacheHistory::class.java )
                    res.add(m)
                }
            } catch (e: Exception){ }

            return res
        }
    }
}
package com.prueba.liverpool.views

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import com.prueba.liverpool.R
import com.prueba.liverpool.adapters.HistorySearchAdapter
import com.prueba.liverpool.adapters.ProductAdapter
import com.prueba.liverpool.interfaces.IProduct
import com.prueba.liverpool.models.MProductMain
import com.prueba.liverpool.presenters.PProduct
import com.prueba.liverpool.util.Tools
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.menu_search.*
import kotlinx.android.synthetic.main.view_generic_list.*


class VHome : VCommon(), IProduct.View {

    private lateinit var presenter: PProduct
    private var adapter: ProductAdapter? = null
    private var context: Context? = null
    private var adapterCache: HistorySearchAdapter? = null

    //Default Page
    private var startPage = 1
    private var endPage = 20

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupView()
    }

    private fun setupView() {
        setSupportActionBar(toolbar)
        context = this
        presenter = PProduct(this)
        adapterCache = HistorySearchAdapter(context!!, getSearchCache())
        rvHistorySearch.adapter = adapterCache

        msEtSearch.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (rvHistorySearch.visibility == View.GONE)
                rvHistorySearch.visibility = View.VISIBLE
            }
        })

        msEtSearch.setOnKeyListener(View.OnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                Tools.hideKeyboard(this)
                getData(msEtSearch.text!!.toString())
                rvHistorySearch.visibility = View.GONE
                return@OnKeyListener true
            }
            false
        })
    }

    fun setQueryAdapter(query: String){
        msEtSearch.setText(query)
    }

    fun getData(query: String = "") {
        llLoading.visibility = View.VISIBLE
        llEmpty.visibility = View.GONE
        llError.visibility = View.GONE
        rvRecycler.visibility = View.GONE
        setSearchCache(query)
        adapterCache = HistorySearchAdapter(context!!, getSearchCache())
        rvHistorySearch.adapter = adapterCache
        presenter.searchProduct(query, startPage, endPage)

    }

    override fun successProduct(model: MProductMain) {
        llLoading.visibility = View.GONE

        if (model.arrProducts!!.isEmpty()) {
            llEmpty.visibility = View.VISIBLE
        } else {
            adapter = ProductAdapter(this, model.arrProducts!!)
            rvRecycler.adapter = adapter
            rvRecycler.visibility = View.VISIBLE
        }
    }

    override fun errorProduct(error: String) {
        llLoading.visibility = View.GONE
        llError.visibility = View.VISIBLE
    }
}
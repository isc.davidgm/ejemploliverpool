package com.prueba.liverpool.models

import com.google.gson.annotations.SerializedName

class MProduct {

    @SerializedName("productId")
    var productId: String = ""
    @SerializedName("skuRepositoryId")
    var skuRepositoryId: String = ""
    @SerializedName("productDisplayName")
    var productDisplayName: String = ""
    @SerializedName("productType")
    var productType: String = ""
    @SerializedName("productRatingCount")
    var productRatingCount: Int = 0
    @SerializedName("productAvgRating")
    var productAvgRating: Float = 0f
    @SerializedName("listPrice")
    var listPrice: Float = 0f
    @SerializedName("minimumListPrice")
    var minimumListPrice: Float = 0f
    @SerializedName("maximumListPrice")
    var maximumListPrice: Float = 0f
    @SerializedName("promoPrice")
    var promoPrice: Float = 0f
    @SerializedName("minimumPromoPrice")
    var minimumPromoPrice: Float = 0f
    @SerializedName("maximumPromoPrice")
    var maximumPromoPrice: Float = 0f
    @SerializedName("isHybrid")
    var isHybrid: Boolean = false
    @SerializedName("isMarketPlace")
    var isMarketPlace: Boolean = false
    @SerializedName("isImportationProduct")
    var isImportationProduct: Boolean = false
    @SerializedName("brand")
    var brand: String = ""
    @SerializedName("seller")
    var seller: String = ""
    @SerializedName("category")
    var category: String = ""
    @SerializedName("smImage")
    var smImage: String = ""
    @SerializedName("lgImage")
    var lgImage: String = ""
    @SerializedName("xlImage")
    var xlImage: String = ""
    @SerializedName("groupType")
    var groupType: String = ""
    //Pending
    //@SerializedName("plpFlags")
    //var plpFlags: Array<>? = null
    @SerializedName("variantsColor")
    var arrColor: Array<MProductColor>? = null
}
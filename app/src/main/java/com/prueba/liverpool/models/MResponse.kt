package com.prueba.liverpool.models

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import com.prueba.liverpool.network.Petition
import com.prueba.liverpool.util.DefaultExclusionStrategy
import org.json.JSONObject

class MResponse {
    @SerializedName("status")
    var status: MStatus = MStatus()
    @SerializedName("pageType")
    var pageType: String = ""
    @SerializedName("plpResults")
    var plpResults: MPlpResults = MPlpResults()
    var isOk: Boolean = false

    var dsResponse: JSONObject? = null

    fun response(response: String?): MResponse? {

        var res: MResponse? = null
        val builder = GsonBuilder()
        builder.setExclusionStrategies(DefaultExclusionStrategy())
        val gson = builder.create()
        try {
            res = gson.fromJson(response, MResponse::class.java)
            res.isOk = res.status.status == Petition.TAG_IS_OK
        } catch (e: Exception) {
            res = MResponse()
            res.isOk = false
        }

        try {
            val obj = JSONObject(response!!)
            if (!obj.isNull("plpResults")) {
                res!!.dsResponse = obj.getJSONObject("plpResults")
            }
        } catch (e: Exception) {
        }
        return res
    }
}

class MStatus {
    @SerializedName("status")
    var status: String = ""
    @SerializedName("statusCode")
    var statusCode: Int = -1

    //{"status":{"status":"Bad Request","errorDescription":"Ocurrió un error, favor de intentarlo nuevamente.","statusCode":1}}
}

class MPlpResults {
    @SerializedName("categoryId")
    var categoryId: String = ""
    @SerializedName("currentSortOption")
    var currentSortOption: String = ""
    @SerializedName("currentFilters")
    var currentFilters: String = ""
    @SerializedName("firstRecNum")
    var firstRecNum: Int = 0
    @SerializedName("lastRecNum")
    var lastRecNum: Int = 0
    @SerializedName("recsPerPage")
    var recsPerPage: Int = 0
    @SerializedName("totalNumRecs")
    var totalNumRecs: Int = 0
    @SerializedName("originalSearchTerm")
    var originalSearchTerm: String = ""
}
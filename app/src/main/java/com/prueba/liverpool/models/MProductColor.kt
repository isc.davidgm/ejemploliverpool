package com.prueba.liverpool.models

import com.google.gson.annotations.SerializedName

class MProductColor {
    @SerializedName("colorName")
    var colorName: String = ""
    @SerializedName("colorHex")
    var colorHex: String = ""
    @SerializedName("colorImageURL")
    var colorImageURL: String = ""
}
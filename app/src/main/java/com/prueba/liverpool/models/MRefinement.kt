package com.prueba.liverpool.models

import com.google.gson.annotations.SerializedName

class MRefinement {
    @SerializedName("count")
    var count: Int = 0
    @SerializedName("label")
    var label: String = ""
    @SerializedName("refinementId")
    var refinementId: String = ""
    @SerializedName("selected")
    var selected: Boolean = false
}
package com.prueba.liverpool.models

import com.google.gson.annotations.SerializedName

class MProductMain {
    @SerializedName("sortOptions")
    var arrSortOptions: Array<MOptions>? = null

    @SerializedName("refinementGroups")
    var arrRefinementGroups: Array<MRefinementGroups>? = null

    @SerializedName("records")
    var arrProducts: Array<MProduct>? = null
}
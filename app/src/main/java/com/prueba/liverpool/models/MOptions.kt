package com.prueba.liverpool.models

import com.google.gson.annotations.SerializedName

class MOptions {
    @SerializedName("sortBy")
    var sortBy: String = ""
    @SerializedName("label")
    var label: String = ""
}
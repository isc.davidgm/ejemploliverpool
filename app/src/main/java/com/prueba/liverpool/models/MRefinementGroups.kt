package com.prueba.liverpool.models

import com.google.gson.annotations.SerializedName

class MRefinementGroups {
    @SerializedName("name")
    var name: String = ""
    @SerializedName("multiSelect")
    var multiSelect: Boolean = false
    @SerializedName("dimensionName")
    var dimensionName: String = ""
    @SerializedName("refinement")
    var arrRefinement: Array<MRefinement>? = null
}
package com.prueba.liverpool.util

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager

class Tools {
    companion object {
        fun hideKeyboard(activity: Activity) {
            try {
                val imm = activity.getSystemService(
                    Activity.INPUT_METHOD_SERVICE
                ) as InputMethodManager
                var view = activity.currentFocus
                if (view == null) {
                    view = View(activity)
                }
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            } catch (e: Exception) {
            }
        }

        fun getMoney(money: Float): String{
            var cash = money.toString()
            cash = try {
                val formatted = String.format("%.2f", cash.toFloatOrNull())
                "$${formatted.replace(",", ".")}"
            } catch (e: Exception) {
                "$${money}".toUpperCase()
            }

            return cash
        }
    }
}